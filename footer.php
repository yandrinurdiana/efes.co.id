		<!-- Footer
		============================================= -->
		<footer id="footer" class="dark"  style="margin-top: 0px;">

			<div class="container">

				<!-- Footer Widgets
				============================================= -->
				<div class="footer-widgets-wrap clearfix">

					<div class="col_two_third">

						<div class="col_one_third">

							<div class="widget clearfix">

								<img src="assets/images/footer-widget-logo.png" alt="" class="footer-logo">

								<p><strong>PT. INDAH KEJORA ABADI</strong></p>

								<div style="background: url('assets/images/world-map.png') no-repeat center center; background-size: 100%;">
									<address>
										<strong>HEAD OFFICE:</strong><br>
										Gedung Bank Natasha Lt. 3<br>
										Jl. Urip Sumoharjo. No. 65, Yogyakarta<br>
										<b>Call Center :</b> (0274) 564251<br> 
									</address>
									<address>
										<strong>MARKETING OFFICE:</strong><br>
										Gedung Navagreen Lt. 4<br>
										Jl. Tanjung Duren Raya No. 137 a-c, Jakarta Barat<br>
										<b>Call Center :</b> (021) 29449099<br> 
										<b>Fax:</b> (021) 29449098 <br> 
									</address>
								</div>

							</div>

						</div>

						<div class="col_one_third">

							<div class="widget clearfix">

								<h4>BANK ACCOUNT</h4>

								<div>
									
									<address>
										<strong>BANK MANDIRI</strong><br>
										<img src="assets/images/logo_mandiri.png"><br><br>
										  KCP Diponegoro Yogyakarta<br>
										<b>A.c :</b> 1370000166666<br>
										<b>A.n :</b> PT Alura Data Gemilang<br>										
									</address>
								</div>

							</div>

						</div>

						<div class="col_one_third col_last">

							<div class="widget clearfix">
								<h4>Recent News</h4>

								<div id="post-list-footer">
									
									<div class="spost clearfix">
										<div class="entry-c">
											<div class="entry-title">
												<h4><a href="recent-new">BOP (Businnes Opportunity Presentation) di Gedung Bank Natasha, Lt. 3, Jl. Urip Sumoharjo. No. 65, YOGYAKARTA</a></h4>
											</div>
											<ul class="entry-meta">
												<li>Setiap Hari Rabu dan Jum'at</li>
												<!--<li>14.00 s/d Selesai</li>-->
											</ul>
										</div>
									</div>

									<div class="spost clearfix">
										<div class="entry-c">
											<div class="entry-title">
												<h4><a href="recent-new">BOP (Businnes Opportunity Presentation) di Gedung Navagreen Lt. 4, Jl. Tanjung Duren Raya No. 137 a-c, JAKARTA</a></h4>
											</div>
											<ul class="entry-meta">
												<li>Setiap Hari Selasa dan Kamis</li>
												<!--<li>14.00 s/d Selesai</li>-->
											</ul>
										</div>
									</div>
								</div>
							</div>

						</div>

					</div>

					<div class="col_one_third col_last">

						<div class="widget clearfix" style="margin-bottom: -20px;">

							<div class="row">

								<div class="col-md-6 bottommargin-sm">
									<div><b>SITEMAP</b></div><br>
								<ul>
									<li><a href="http://localhost/efes_frontend">HOME</a></li>
									<!-- <li><a href="//efes.co.id/marketing-plan">Marketing Plan</a></li> -->
									<li><a href="#">Testimoni</a></li>
									<li><a href="gallery">Gallery</a></li>
									</ul>
								</div>

								<div class="col-md-6 bottommargin-sm">
									<div><b>SITEMAP</b></div><br>
								<ul>
									<li><a href="about">About Us</a></li>
									<li><a href="http://localhost">LOGIN</a></li>
								</ul>
								</div>

							</div>

						</div>

						<div class="widget subscribe-widget clearfix">
							<h5><strong>Subscribe</strong> to Our Newsletter to get Important News &amp; Amazing Offers :</h5>
							<div class="widget-subscribe-form-result"></div>
							<form id="newsletter" name="newsletter" action="subscribe.php" role="form" method="post" class="nobottommargin">
								<div class="input-group divcenter">
									<span class="input-group-addon"><i class="icon-envelope"></i></span>
									<input type="email" id="widget-subscribe-form-email" name="subscribe-email" id="subscribe-email" class="form-control required email" placeholder="Enter your Email">
									<span>
									<input type="submit" value="Subscribe" name="signup-button" id="signup-button">
									</span>
								</div>
							</form>
						</div>

					</div>

				</div><!-- .footer-widgets-wrap end -->

			</div>

			<!-- Copyrights
			============================================= -->
			<div id="copyrights">

				<div class="container clearfix">

					<div class="col_half">
						Copyrights &copy; 2017 efes.co.id<br>
						<div class="copyright-links"><a href="#">Terms of Use</a> / <a href="#">Privacy Policy</a></div>
					</div>

					<div class="col_half col_last tright">
						<div class="fright clearfix">
							<a href="https://www.facebook.com/efes.efes.733" class="social-icon si-small si-borderless si-facebook">
								<i class="icon-facebook"></i>
							</a>

							<a href="#" class="social-icon si-small si-borderless si-twitter">
								<i class="icon-twitter"></i>
							</a>

							<a href="#" class="social-icon si-small si-borderless si-pinterest">
								<i class="icon-pinterest"></i>
							</a>

							<a href="#" class="social-icon si-small si-borderless si-github"> 
								<i class="icon-github"></i>
							</a>

							<a href="#" class="social-icon si-small si-borderless si-linkedin">
								<i class="icon-linkedin"></i>
							</a>
						</div>

						<div class="clear"></div>

						<i class="icon-envelope"></i> customercare@efes.co.id <span class="middot">&middot;</span> <i class="icon-phone"></i> (0274) 564251
					</div>

				</div>

			</div><!-- #copyrights end -->

		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
	<script type="text/javascript" src="assets/js/jquery.js"></script>

        <script>
//            $("document").ready(function(){
//            $(".pf-smrg activeFilter portfolio-filter portfolio-filter clearfix").click(function().trigger("click");
            
            
            $("document").ready({
                $(".pf-smrg activeFilter portfolio-filter portfolio-filter clearfix").click(function(){alert('i work click triggered'});
            }); 

            $("document").ready(function() { 
                $(".pf-smrg activeFilter portfolio-filter portfolio-filter clearfix").trigger('click'); 
            });
        });</script>
        
        
	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="assets/js/functions.js"></script>

	<!-- particles.js lib (JavaScript CodePen settings): https://github.com/VincentGarreau/particles.js -->
	<script type="text/javascript" src='https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js'></script>
	<script type="text/javascript" src="assets/particles/js/index.js"></script>
