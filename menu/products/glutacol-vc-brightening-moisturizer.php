<?php
include "../../sub-header.php";
?>
		<!-- Page Title
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<h1>Brightening</h1>
				<span>Glutacol VC Brightening Moisturizer</span>
				<ol class="breadcrumb">
					<li><a href="#">Category</a></li>
					<li class="active">Brightening</li>
					<li class="active">Glutacol VC Brightening Moisturizer</li>
				</ol>
			</div>

		</section><!-- #page-title end --> 

		<!-- Content
		============================================= -->
<div id="section-product-detail">

	<div class="section-pr section-page section-lg">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
						<img src="assets/images/products/brightening.png" alt="Glutacol VC Brightening Moisturizer"/>
				</div>
				<div class="col-sm-8">
					<div class="post-detail">
						<h1><font color="white">Glutacol VC Brightening Moisturizer</font></h1>
						<!--<br/>-->
						NA18161900362<br>
						40 gr<br><br>
						
						<p style="text-align: justify; ">
						<b>EFES Glutacol VC Brightening Moisturizer </b>mengandung gluthatione, vitamin C, kolagen, probiotik, yang diolah dengan 
						proses enkapsulasi dan <i>nano technology</i>, juga mengandung asam amino yang berfungsi sebagai moisturizer. 
						Antioksidan botanikal ekstrak kakadu berfungsi sebagai anti aging dan pencerah kulit.
<!--                                                Dikombinasikan dengan ekstrak <i>phytoplacenta </i>dan ekstrak bunga <i>daisy </i>yang efektif mencerahkan,
                                                membantu menyamarkan noda hitam, dan ruam kulit yang disebabkan sinar UV.-->
                                                &nbsp;</p>
						
						<!--<p style="text-align: justify;">-->
<!--						Kandungan&nbsp;<i>magic white-</i>nya membantu kulit menjadi bercahaya. Kombinasi bahan-bahan yang terdapat dalam&nbsp;
						<b>EFES Glutacol VC Brightening Moisturizer&nbsp;</b>menjadikan kulit terasa lembut dan bercahaya.<br></p>						
						<br/>-->
						
                                                <div class="col-sm-4">
                                                        <img src="assets/images/bright.png">
                                                </div>
                                                <div class="col-sm-8">
                                                        <div class="post-detail">
                                                                <!--<h2>CARA KERJA Glutacol VC Brightening Moisturizer</h2>-->
                                                                <br/>			
                                                                <p style="text-align: justify;">
                                                                <!--<p style="text-align: justify;">-->
                                                                Kandungan&nbsp;<i>magic white-</i>nya membantu kulit menjadi bercahaya. Kombinasi bahan-bahan yang terdapat dalam&nbsp;
                                                                <b>EFES Glutacol VC Brightening Moisturizer&nbsp;</b>menjadikan kulit terasa lembut dan bercahaya.<br>
                                                                Kombinasi ekstrak <i>phytoplacenta</i> dan ekstrak bunga <i>daisy</i> efektif mencerahkan,
                                                                membantu menyamarkan noda hitam, dan ruam kulit yang disebabkan sinar UV.<br></p>					
                                                                </div>
                                                </div>
<!--						<h2>Cara Pemakaian</h2>
			
                                                <p style="text-align: justify; ">
						Oleskan krim secara merata pada wajah setiap pagi dan malam hari setelah wajah dibersihkan<br></p>						
						<br/>

						<h2>Harga</h2>
						<b>Harga Jual : Rp. 1.740.000<br>Harga Konsultan : Rp. 1.450.000</b>-->

					</div>
				</div>
			</div>
		</div>
	</div>
<!--	 <div class="section nomargin noborder" style="background-color:#e1e1e1;text-shadow: 2px 2px #aeaeae;">
					<div class="heading-block center nobottomborder nobottommargin">
						<h2><font color="#40442b">"Cara Kerja Glutacol VC Brightening Moisturizer"</font></h2>
					</div>
				</div>
	<div class="section-pr section-lg">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<img src="assets/images/bright.png">
				</div>
				<div class="col-sm-8">
					<div class="post-detail">
						<h2>CARA KERJA Glutacol VC Brightening Moisturizer</h2>
						<br/>			
						<p style="text-align: justify;">
						Kombinasi ekstrak&nbsp;<i>phytoplacenta&nbsp;</i>dan ekstrak bunga&nbsp;<i>daisy&nbsp;</i>
						efektif mencerahkan, membantu menyamarkan noda hitam, dan ruam kulit yang disebabkan sinar UV.<br></p>					
						</div>
				</div>
			</div>
		</div>
	</div>	-->
    <!--<div class="section nomargin noborder" style="background-color:#e1e1e1;text-shadow: 2px 2px #aeaeae;">-->
                        <section id="pmbts_product">
                            <div class="container clearfix">
					<div class="heading-block center nobottomborder nobottommargin">
						<h2><font color="#40442b">"List Product"</font></h2>
					</div>
                            </div>
                        </section>
		<div class="section-pr section-lg text-center" id="produk-terkait">
		<div class="container">
			<!--<h2 class="heading">LIST PRODUCTS</h2>-->
			<!--<hr class="bold"/><br/>-->
			<div class="row">			
			<div class="col-md-4" data-animate="bounceIn">			
					<div class="product-item">
						<a href="../products/glutacol-vc-cleansing-peel-gel">
						<img src="assets/images/products/cleanser1.png" alt="Glutacol VC Cleansing Peel Gel"></a>
						<div class="product-title">Glutacol VC Cleansing Peel Gel</div>
					</div>
				</div>
				
			<div class="col-md-4" data-animate="bounceIn">
					<div class="product-item">
						<a href="../products/glutacol-vc-renewal-fluffy-mask">
						<img src="assets/images/products/Glutacol-VC-Renewal-Fluffy-Mask.png" alt="Glutacol VC Renewal Fluffy Mask"></a>
						<div class="product-title">Glutacol VC Renewal Fluffy Mask</div>
					</div>
				</div>
				
			<div class="col-md-4" data-animate="bounceIn">
					<div class="product-item">
						<a href="../products/glutacol-vc-O2-bubble">
						<img src="assets/images/products/cleanser2.png" alt="Glutacol VC O2 (Oxygen) Bubble Facial Cleanser"></a>
						<div class="product-title">Glutacol VC O2 (Oxygen) Bubble Facial Cleanser</div>
					</div>
				</div>				

			</div>
		</div>
	</div>
	
	</div>

<?php
include "../../footer.php";
?>