<?php
include "../../sub-header.php";
?>
		<!-- Page Title
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<h1>Cleanser</h1>
				<span>Glutacol VC Cleansing Peel Gel</span>
				<ol class="breadcrumb">
					<li><a href="#">Category</a></li>
					<li class="active">Cleanser</li>
					<li class="active">Glutacol VC Cleansing Peel Gel</li>
				</ol>
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
<div id="section-product-detail">

	<div class="section-pr section-page section-lg">
		<div class="container">
			<div class="row">
				<div class="col-sm-4" align="center">
						<img  src="assets/images/products/cleanser1.png" alt="Glutacol VC O2 (Oxygen) Bubble Facial Cleanser"/>
				</div>
				<div class="col-sm-8">
					<div class="post-detail">
						<h1><font color="white">Glutacol VC Cleansing Peel Gel</font></h1>
						<!--<br/>-->
						NA 18161204215<br>
						100 gr<br><br>
						
						<p style="text-align: justify; ">
						<b>EFES Glutacol VC Cleansing Peel Gel</b> 
                                                diolah dengan proses <i>enkapsulasi</i> dan <i>nano technology</i>, mengandung gluthatione, vitamin C, colagen, probiotik. Kandungan 
						<i>ekstrak  beetroot</i>dapat melembabkan kulit. Kombinasi kandungan Natural AHA dari
						<i>Helianthus Annuus (Sunflower) Seed Oil</i> dapat membantu membersihkan dan melembabkan kulit
                                                sehingga kulit menjadi bersih, lembut, dan bercahaya.&nbsp;</p>
                                                
                                                 <div class="col-sm-4">
                                                        <img src="assets/images/peel_gel.png">
                                                </div>
                                                <div class="col-sm-8">
                                                        <div class="post-detail">
                                                                <!--<h2>CARA KERJA Glutacol VC Cleansing Peel Gel</h2>-->
                                                                <br/>			
                                                                <p style="text-align: justify; ">
                                                                Kandungan <i>ekstrak  beetroot</i>&nbsp;dapat melembabkan kulit. Selain itu, kombinasi kandungan Natural AHA dari 
                                                                <i>Helianthus Annuus (Sunflower) Seed Oil</i>&nbsp;dapat membantu membersihkan dan melembabkan kulit
                                                                sehingga kulit menjadi bersih, lembut, dan bercahaya.&nbsp;<br></p>					
                                                        </div>

                                                </div>
						
<!--						<h2>Cara Pemakaian</h2> 
						<p style="text-align: justify; ">
						Tuang secukupnya <b>EFES Glutacol VC Cleansing Peel Gel</b> 
						pada telapak tangan, usapkan dan ratakan pada wajah dan tubuh. 
                                                Gosok lembut dengan gerakan memutar, kemudian bilas dengan air hingga bersih.&nbsp;<br></p><br/>

						<h2>Harga</h2>
                                                <b>Harga Jual : Rp. 1.740.000/(3 Pcs)<br>Harga Konsultan : Rp. 1.450.000/(3 Pcs)</b> &nbsp;-->
                                                <!--<img src="assets/images/products/cleanser1.png" alt="Glutacol VC O2 (Oxygen) Bubble Facial Cleanser"/>-->

					</div>
				</div>
                            
			</div>
                    
		</div>
	</div>
<!--	<div class="section nomargin noborder" style="background-color:#e1e1e1;text-shadow: 2px 2px #aeaeae;">
					<div class="heading-block center nobottomborder nobottommargin">
						<h2><font color="#40442b">"Manfaat Glutacol VC Cleansing Peel Gel"</font></h2>
					</div>
				</div>
	<div class="section-pr section-lg">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<img src="assets/images/peel_gel.png">
				</div>
				<div class="col-sm-8">
					<div class="post-detail">
						<h2>CARA KERJA Glutacol VC Cleansing Peel Gel</h2>
						<br/>			
						<p style="text-align: justify; ">
						Kandungan <i>ekstrak  beetroot</i>&nbsp;dapat melembabkan kulit. Selain itu, kombinasi kandungan Natural AHA dari 
						<i>Helianthus Annuus (Sunflower) Seed Oil</i>&nbsp;dapat membantu membersihkan dan melembabkan kulit
                                                sehingga kulit menjadi bersih, lembut, dan bercahaya.&nbsp;<br></p>					
					</div>

				</div>
			</div>
		</div>
	</div>	-->
    <!--<div class="section nomargin noborder" style="background-color:#e1e1e1;text-shadow: 2px 2px #aeaeae;">-->
                            <section id="pmbts_product">
                                <div class="container clearfix">
					<div class="heading-block center nobottomborder nobottommargin">
						<h2><font color="#40442b">"List Product"</font></h2>
					</div>
				</div>
                            </section>
		<div class="section-pr section-lg text-center" id="produk-terkait">
		<div class="container">
			<!--<h2 class="heading">LIST PRODUCTS</h2>-->
			<!--<hr class="bold"/><br/>-->
			<div class="row">			
			<div class="col-md-4" data-animate="bounceIn">			
					<div class="product-item">
						<a href="../products/glutacol-vc-renewal-lift">
						<img src="assets/images/products/anti_aging.png" alt="Glutacol VC O2 (Oxygen) Bubble Facial Cleanser"></a>
						<div class="product-title">Glutacol VC Renewal Lift</div>
					</div>
				</div>
				
			<div class="col-md-4" data-animate="bounceIn">
					<div class="product-item">
						<a href="../products/glutacol-vc-O2-bubble">
						<img src="assets/images/products/cleanser2.png" alt="Glutacol VC Renewal Fluffy Mask"></a>
						<div class="product-title">Glutacol VC O2 (Oxygen) Bubble Facial Cleanser</div>
					</div>
				</div>
				
			<div class="col-md-4" data-animate="bounceIn">
					<div class="product-item">
						<a href="../products/glutacol-vc-brightening-moisturizer">
						<img src="assets/images/products/brightening.png" alt="Glutacol VC Brightening Moisturizer"></a>
						<div class="product-title">Glutacol VC Brightening Moisturizer</div>
					</div>
				</div>				

			</div>
		</div>
	</div>
	
	</div>

<?php
include "../../footer.php";
?>