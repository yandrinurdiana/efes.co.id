<?php
include "../../sub-header.php";
?>
		<!-- Page Title
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<h1>Body Care</h1>
				<span>Glutacol VC Body Moisturizer Foam</span>
				<ol class="breadcrumb">
					<li><a href="#">Category</a></li>
					<li class="active">Body Care</li>
					<li class="active">Glutacol VC Body Moisturizer Foam</li>
				</ol>
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
<div id="section-product-detail">

	<div class="section-pr section-page section-lg">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
						<img src="assets/images/products/body_care.png" alt="Glutacol VC Body Moisturizer Foam"/>
				</div>
				<div class="col-sm-8">
					<div class="post-detail">
						<h1><font color="white">Glutacol VC Body Moisturizer Foam</font></h1>
						<!--<br/>-->
						NA 18160700938<br>
						120 gr<br><br> 
						
						<p style="text-align: justify; ">
						<b>EFES Glutacol VC Body Moisturizer Foam</b> 
                                                diolah dengan proses <i>enkapsulasi</i> dan <i>nano technology</i>, mengandung gluthatione, vitamin C, colagen, probiotik. Kandungan 
						<i>Greentea Oil</i>-nya berfungsi sebagai antioksidan, antiseptik, antibakteri dan untuk melembabkan. Kombinasi kandungan Natural AHA dari
						<i>Olive Oil</i> yang mampu melindungi kulit dari kerusakan akibat sinar UV, dan mengurangi peradangan.&nbsp;</p>
						
                                                <div class="col-sm-4">
                                                        <img src="assets/images/body_foam.png">
                                                </div>
                                                <div class="col-sm-8">
                                                        <div class="post-detail">
                                                                <!--<h2>CARA KERJA Glutacol VC O2 (Oxygen) Bubble Facial Cleanser</h2>-->
                                                                <br/>			
                                                                <p style="text-align: justify; ">
                                                                Kandungan <i>Greentea Oil</i>&nbsp;-nya berfungsi sebagai antioksidan, antiseptik, antibakteri dan untuk melembabkan. Selain itu, kombinasi kandungan Natural AHA dari 
                                                                <i>Olive Oil</i>&nbsp;yang mampu melindungi kulit dari kerusakan akibat sinar UV, dan mengurangi peradangan.&nbsp;<br></p>					
                                                        </div>

                                                </div>
<!--						<h2>Cara Pemakaian</h2> 
						<p style="text-align: justify; ">
						Tuang secukupnya <b>EFES Glutacol VC Body Moisturizer Foam</b> 
						pada telapak tangan, dengan menekan pompa foam (dalam  bentuk busa).
                                                Usapkan secara merata pada seluruh tubuh. Gunakan satu atau dua kali dalam sehari.&nbsp;<br></p><br/>

						<h2>Harga</h2>
						<b>Harga Jual : Rp. 1.740.000/(3 Pcs)<br>Harga Konsultan : Rp. 1.450.000/(3 Pcs)</b>-->

					</div>
				</div>
			</div>
		</div>
	</div>
                        <!--	<section id="pmbts_product">
                                        <div class="container clearfix">
                                                <div class="section nomargin noborder" style="background-color:#e1e1e1;text-shadow: 2px 2px #aeaeae;">
                                                                <div class="heading-block center nobottomborder nobottommargin">
                                                                        <h2><font color="#40442b">"Cara Kerja Glutacol VC Body Moisturizer Foam"</font></h2>
                                                                </div>
                                        </div>
                                </section>
                                <div class="section-pr section-lg">
                                        <div class="container">
                                                <div class="row">
                                                        <div class="col-sm-4">
                                                                <img src="assets/images/body_foam.png">
                                                        </div>
                                                        <div class="col-sm-8">
                                                                <div class="post-detail">
                                                                        <h2>CARA KERJA Glutacol VC O2 (Oxygen) Bubble Facial Cleanser</h2>
                                                                        <br/>			
                                                                        <p style="text-align: justify; ">
                                                                        Kandungan <i>Greentea Oil</i>&nbsp;-nya berfungsi sebagai antioksidan, antiseptik, antibakteri dan untuk melembabkan. Selain itu, kombinasi kandungan Natural AHA dari 
                                                                        <i>Olive Oil</i>&nbsp;yang mampu melindungi kulit dari kerusakan akibat sinar UV, dan mengurangi peradangan.&nbsp;<br></p>					
                                                                </div>

                                                        </div>
                                                </div>
                                        </div>
                                </div>	-->
                    <section id="pmbts_product">
                        <div class="container clearfix">
     <!--<div class="section nomargin noborder" style="background-color:#e1e1e1;text-shadow: 2px 2px #aeaeae;">-->
                        <div class="heading-block center nobottomborder nobottommargin">
                                <h2><font color="#40442b">"List Product"</font></h2>
                        </div>
                    </section>
		<div class="section-pr section-lg text-center" id="produk-terkait">
		<div class="container">
			<!--<h2 class="heading">LIST PRODUCTS</h2>-->
			<!--<hr class="bold"/><br/>-->
			<div class="row">			
			<div class="col-md-4" data-animate="bounceIn">			
					<div class="product-item">
						<a href="../products/glutacol-vc-renewal-lift">
						<img src="assets/images/products/anti_aging.png" alt="Glutacol VC O2 (Oxygen) Bubble Facial Cleanser"></a>
						<div class="product-title">Glutacol VC Renewal Lift</div>
					</div>
				</div>
				
			<div class="col-md-4" data-animate="bounceIn">
					<div class="product-item">
						<a href="../products/glutacol-vc-renewal-fluffy-mask">
						<img src="assets/images/products/Glutacol-VC-Renewal-Fluffy-Mask.png" alt="Glutacol VC Renewal Fluffy Mask"></a>
						<div class="product-title">Glutacol VC Renewal Fluffy Mask</div>
					</div>
				</div>
				
			<div class="col-md-4" data-animate="bounceIn">
					<div class="product-item">
						<a href="../products/glutacol-vc-brightening-moisturizer">
						<img src="assets/images/products/brightening.png" alt="Glutacol VC Brightening Moisturizer"></a>
						<div class="product-title">Glutacol VC Brightening Moisturizer</div>
					</div>
				</div>				

			</div>
		</div>
	</div>
	
	</div>

<?php
include "../../footer.php";
?>