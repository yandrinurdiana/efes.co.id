<?php
include "../../sub-header.php";
?>
		<!-- Page Title
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<h1>Anti Aging</h1>
				<span>Glutacol VC Renewal Lift</span>
				<ol class="breadcrumb">
					<li><a href="#">Category</a></li>
					<li class="active">Anti Aging</li>
					<li class="active">Glutacol VC Renewal Lift</li>
				</ol>
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
<div id="section-product-detail">

	<div class="section-pr section-page section-lg">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
						<img src="assets/images/products/anti_aging.png" alt="Glutacol VC Renewal Lift"/>
				</div>
				<div class="col-sm-8">
					<div class="post-detail">
						<h1><font color="white">Glutacol VC Renewal Lift</font></h1>
						<!--<br/>-->
						NA18162000237<br>
						40 gr<br><br>
						
						<p style="text-align: justify; ">
						<b>EFES Glutacol VC Renewal Lift</b>
						mengandung gluthatione, kolagen, dan probiotik, yang diolah dengan dengan proses enkapsulasi dan 
						<i>nano technology</i>, juga mengandung asam amino yang berfungsi sebagai anti aging, <i>moisturizer</i>, dan pencerah kulit. 
						<i>Bifida ferment lysate</i>-nya dapat membantu mencegah terjadinya kerusakan kulit yang disebabkan oleh sinar UV. <br></p>
						
                                                 <div class="col-sm-4">
                                                        <img src="assets/images/red_droplet.png">
                                                </div>
                                                <div class="col-sm-8">
                                                    <p style="text-align: justify;">
                                                    Dikombinasikan ekstrak&nbsp;<i>centella&nbsp;</i>efektif sebagai&nbsp;
                                                    <i>anti-aging</i>&nbsp;yang membantu pembentukan kolagen dan<i>&nbsp;red algae</i>&nbsp;yang membantu mengencangkan kulit. 
                                                    Kombinasi bahan-bahannya menjadikan EFES Glutachol VC Renewal Lift efektif sebagai anti aging.<br></p><br/>
                                                </div>
                                               
<!--						<h2>Cara Pemakaian</h2>
						<p style="text-align: justify; ">
						Oleskan <b>EFES Glutacol VC Renewal Lift</b>&nbsp;pada wajah yang telah dibersihkan setiap pagi dan malam.<br></p>		
						 <br/>

						<h2>Harga</h2>
						<b>Harga Jual : Rp. 1.740.000<br>Harga Konsultan : Rp. 1.450.000</b>-->

					</div>
				</div>
			</div>
		</div>
	</div>
                <!--<div class="section nomargin noborder" style="background-color:#e1e1e1;">-->
<!--                <section id="pmbts_product">
                <div class="container clearfix">
                        <div class="heading-block center nobottomborder nobottommargin">
                                <h2><font color="#40442b">"Cara Kerja Glutacol VC Renewal Lift"</font></h2>
                        </div>
                </div>
                </section>
	<div class="section-pr section-lg">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<img src="assets/images/red_droplet.png">
				</div>
				<div class="col-sm-8">
					<div class="post-detail">
						<h2>CARA KERJA Glutacol VC Renewal Lift</h2>
						<br/>			
						<p style="text-align: justify; ">Kombinasi ekstrak&nbsp;<span style="font-style: italic;">centella </span>efektif sebagai <span style="font-style: italic;">anti-aging</span> yang membantu pembentukan kolagen dan<span style="font-style: italic;">&nbsp;red algae</span>&nbsp;yang membantu mengencangkan kulit.<br></p>					</div>

				</div>
			</div>
		</div>
	</div>	-->
                    <!--<div class="section nomargin noborder" style="background-color:#e1e1e1;">-->
                <section id="pmbts_product">
                    <div class="container clearfix">
                            <div class="heading-block center nobottomborder nobottommargin">
                                    <h2><font color="#40442b">"List Product"</font></h2>
                            </div>
                    </div>
                </section>
		<div class="section-pr section-lg text-center" id="produk-terkait">
		<div class="container">
			<!--<h2 class="heading">LIST PRODUCTS</h2>-->
			<!--<hr class="bold"/><br/>-->
			<div class="row">			
			<div class="col-md-4" data-animate="bounceIn">			
					<div class="product-item">
						<a href="../products/glutacol-vc-body-moizturizer-foam">
						<img src="assets/images/products/body_care.png" alt="Glutacol VC Body Moisturizer Foam"></a>
						<div class="product-title">Glutacol VC Body Moisturizer Foam</div>
					</div>
				</div>
				
			<div class="col-md-4" data-animate="bounceIn">
					<div class="product-item">
						<a href="../products/glutacol-vc-renewal-fluffy-mask">
						<img src="assets/images/products/Glutacol-VC-Renewal-Fluffy-Mask.png" alt="Glutacol VC Renewal Fluffy Mask"></a>
						<div class="product-title">Glutacol VC Renewal Fluffy Mask</div>
					</div>
				</div>
				
			<div class="col-md-4" data-animate="bounceIn">
					<div class="product-item">
						<a href="../products/glutacol-vc-brightening-moisturizer">
						<img src="assets/images/products/brightening.png" alt="Glutacol VC Brightening Moisturizer"></a>
						<div class="product-title">Glutacol VC Brightening Moisturizer</div>
					</div>
				</div>				

			</div>
		</div>
	</div>
	
	</div>

<?php
include "../../footer.php";
?>