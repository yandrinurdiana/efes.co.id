<?php
include "../../sub-header.php";
?>
		<!-- Page Title
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<h1>Mask</h1>
				<span>Glutacol VC Renewal Fluffy Mask</span>
				<ol class="breadcrumb">
					<li><a href="#">Category</a></li>
					<li class="active">Mask</li>
					<li class="active">Glutacol VC Renewal Fluffy Mask</li>
				</ol>
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
<div id="section-product-detail">

	<div class="section-pr section-page section-lg">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
						<img src="assets/images/products/mask.png" alt="Glutacol VC Renewal Lift"/>
				</div>
				<div class="col-sm-8">
					<div class="post-detail">
						<h1><font color="white">Glutacol VC Renewal Fluffy Mask</font></h1>
						<!--<br/>-->
						NA 18160200452<br>
						50 gr<br><br>
						
						<p style="text-align: justify; ">
						<b>EFES Glutacol VC Renewal Fluffy Mask</b> 
						diolah dengan proses enkapsulasi dan <i>nano technology</i>, mengandung gluthatione, vitamin C, colagen, probiotik. 
<!--						<i> Kandungan repair complex</i>-nya dapat membantu memperbaiki kulit. Kombinasi kandungan Natural AHA dari
						<i> Caviar Lime Extract</i> yang mampu mencerahkan dan membuat kulit jadi terasa lebih halus.&nbsp;-->
                                                </p>
						
                                                <div class="col-sm-4">
                                                        <img src="assets/images/masker.png">
                                                </div>
                                                <div class="col-sm-8">
                                                        <div class="post-detail">
                                                                <!--<h2>CARA KERJA Glutacol VC Renewal Fluffy Mask</h2>-->
                                                                <br/>			
                                                                <p style="text-align: justify; ">
                                                                Kandungan <i>repair complex</i>&nbsp;membantu memperbaiki kulit. Selain itu, kombinasi kandungan Natural AHA dari 
                                                                <i>Caviar Lime Extract</i>&nbsp;mampu mencerahkan dan membuat kulit jadi terasa lebih halus.&nbsp;<br></p>					
                                                        </div>

                                                </div>
<!--						<h2>Cara Pemakaian</h2>
						<p style="text-align: justify; ">
						Oleskan <b>EFES Glutacol VC Renewal Fluffy Mask</b> 
						secara merata pada wajah yang telah dibersihkan, diamkan 15-30 menit. 
						Lepaskan masker secara perlahan dari wajah, kemudian bersihkan sisanya dengan menggunkan tissue lembut dan bersih. 
						Hindarkan pemakaian di sekitar mata.&nbsp;<br></p><br/>-->

<!--						<h2>Harga</h2>
						<b>Harga Jual : Rp. 1.740.000<br>Harga Konsultan : Rp. 1.450.000</b>-->

					</div>
				</div>
			</div>
		</div>
	</div>
<!--        <section id="pmbts_product">
                <div class="container clearfix">
    <div class="section nomargin noborder" style="background-color:#e1e1e1;text-shadow: 2px 2px #aeaeae;">
					<div class="heading-block center nobottomborder nobottommargin">
						<h1><font color="#40442b">"Cara Kerja Glutacol VC Renewal Fluffy Mask"</font></h1>
					</div>
				</div>
	</section>
	<div class="section-pr section-lg">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<img src="assets/images/masker.png">
				</div>
				<div class="col-sm-8">
					<div class="post-detail">
						<h2>CARA KERJA Glutacol VC Renewal Fluffy Mask</h2>
						<br/>			
						<p style="text-align: justify; ">
						Kandungan <i>repair complex</i>&nbsp;membantu memperbaiki kulit. Selain itu, kombinasi kandungan Natural AHA dari 
						<i>Caviar Lime Extract</i>&nbsp;mampu mencerahkan dan membuat kulit jadi terasa lebih halus.&nbsp;<br></p>					
					</div>

				</div>
			</div>
		</div>
	</div>	-->
    <section id="pmbts_product">
                <div class="container clearfix">
     <!--<div class="section nomargin noborder" style="background-color:#e1e1e1;text-shadow: 2px 2px #aeaeae;">-->
					<div class="heading-block center nobottomborder nobottommargin">
						<h2><font color="#40442b">"List Product"</font></h2>
					</div>
				</div>
    </section>
		<div class="section-pr section-lg text-center" id="produk-terkait">
		<div class="container">
			<!--<h2 class="heading">LIST PRODUCTS</h2>-->
			<!--<hr class="bold"/><br/>-->
			<div class="row">			
			<div class="col-md-4" data-animate="bounceIn">			
					<div class="product-item">
						<a href="../products/glutacol-vc-renewal-lift">
						<img src="assets/images/products/anti_aging.png" alt="Glutacol VC Renewal Lift"></a>
						<div class="product-title">Glutacol VC Renewal Lift</div>
					</div>
				</div>
				
			<div class="col-md-4" data-animate="bounceIn">
					<div class="product-item">
						<a href="../products/glutacol-vc-O2-bubble">
						<img src="assets/images/products/cleanser2.png" alt="Glutacol VC O2 (Oxygen) Bubble Facial Cleanser"></a>
						<div class="product-title">Glutacol VC O2 (Oxygen) Bubble Facial Cleanser</div>
					</div>
				</div>
				
			<div class="col-md-4" data-animate="bounceIn">
					<div class="product-item">
						<a href="../products/glutacol-vc-brightening-moisturizer">
						<img src="assets/images/products/brightening.png" alt="Glutacol VC Brightening Moisturizer"></a>
						<div class="product-title">Glutacol VC Brightening Moisturizer</div>
					</div>
				</div>				

			</div>
		</div>
	</div>
	
	</div>

<?php
include "../../footer.php";
?>