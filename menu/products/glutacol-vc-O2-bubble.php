<?php
include "../../sub-header.php";
?>
		<!-- Page Title
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<h1>Cleanser</h1>
				<span>Glutacol VC O2 Bubble Facial Cleanser</span>
				<ol class="breadcrumb">
					<li><a href="#">Category</a></li>
					<li class="active">Cleanser</li>
					<li class="active">Glutacol VC O2 Bubble Facial Cleanser</li>
				</ol>
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
<div id="section-product-detail">

	<div class="section-pr section-page section-lg">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
						<img src="assets/images/products/cleanser2.png" alt="Glutacol VC O2 (Oxygen) Bubble Facial Cleanser"/>
				</div>
				<div class="col-sm-8">
					<div class="post-detail">
						<h1><font color="white">Glutacol VC O2 (Oxygen) Bubble Facial Cleanser</font></h1>
						<!--<br/>-->
						NA 18161204215<br>
						100 gr<br><br>
						
						<p style="text-align: justify; ">
						<b>EFES Glutacol VC O2 (Oxygen) Bubble Facial Cleanser</b> 
                                                diolah dengan proses <i>enkapsulasi</i> dan <i>nano technology</i>, mengandung gluthatione, vitamin C, colagen, probiotik. Kandungan 
						<i>Moringa Pterygosperma Seed  Extract</i>-nya berguna untuk ANTI POLUTAN. Kombinasi kandungan Natural AHA dari
						<i>Carthamus Tinctorius (Safflower)  Seed Oil, Olea Europaea (Olive)  Fruit Oil</i> yang mampu membersihkan wajah dari  kotoran dan make up,
                                                menjadikan kulit tampak  lebih bersih segar terawat, dan tetap menjaga  kelembaban kulit wajah.&nbsp;</p>
						
                                                <div class="col-sm-4">
                                                        <img src="assets/images/o2.png">
                                                </div>
                                                <div class="col-sm-8">
                                                        <div class="post-detail">
                                                                <!--<h2>CARA KERJA Glutacol VC O2 (Oxygen) Bubble Facial Cleanser</h2>-->
                                                                <br/>			
                                                                <p style="text-align: justify; ">
                                                                Kandungan <i>Moringa Pterygosperma Seed  Extract</i>&nbsp;Sebagai ANTI POLUTAN. Selain itu, kombinasi kandungan Natural AHA dari 
                                                                <i>Carthamus Tinctorius (Safflower) Seed Oil, Olea Europaea (Olive) Fruit Oil</i>&nbsp;yang mampu membersihkan wajah dari  kotoran dan make up, 
                                                                menjadikan kulit tampak  lebih bersih segar terawat, dan tetap menjaga  kelembaban kulit wajah.&nbsp;<br></p>					
                                                        </div>

                                                </div>
<!--						<h2>Cara Pemakaian</h2> 
						<p style="text-align: justify; ">
						Tuang secukupnya <b>EFES Glutacol VC O2 (Oxygen) Bubble Facial Cleanser</b> 
						pada telapak tangan, usapkan dan ratakan pada wajah dalam keadaan kering tanpa dibasahi terlebih dahulu.
                                                Diamkan  beberapa saat (30-45 detik) sampai terbentuk busa putih merata. 
                                                Selanjutnya hapus dengan tissue lembut atau kapas  kemudian bilas dengan air hingga  bersih.&nbsp;<br></p><br/>

						<h2>Harga</h2>
						<b>Harga Jual : Rp. 1.740.000/(3 Pcs)<br>Harga Konsultan : Rp. 1.450.000/(3 Pcs)</b>-->

					</div>
				</div>
			</div>
		</div>
	</div>
<!--	<div id="pmbts_product">
            <div class="heading-block center nobottomborder nobottommargin">
                    <h2><font color="#40442b">"CARA KERJA Glutacol VC O2 (Oxygen) Bubble Facial Cleanser"</font></h2>
            </div>
        </div>
	<div class="section-pr section-lg">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<img src="assets/images/o2.png">
				</div>
				<div class="col-sm-8">
					<div class="post-detail">
						<h2>CARA KERJA Glutacol VC O2 (Oxygen) Bubble Facial Cleanser</h2>
						<br/>			
						<p style="text-align: justify; ">
						Kandungan <i>Moringa Pterygosperma Seed  Extract</i>&nbsp;Sebagai ANTI POLUTAN. Selain itu, kombinasi kandungan Natural AHA dari 
						<i>Carthamus Tinctorius (Safflower) Seed Oil, Olea Europaea (Olive) Fruit Oil</i>&nbsp;yang mampu membersihkan wajah dari  kotoran dan make up, 
                                                menjadikan kulit tampak  lebih bersih segar terawat, dan tetap menjaga  kelembaban kulit wajah.&nbsp;<br></p>					
					</div>

				</div>
			</div>
		</div>
	</div>	-->
    <!--<div class="section nomargin noborder" style="background-color:#e1e1e1;text-shadow: 2px 2px #aeaeae;">-->
                            <section id="pmbts_product">
                                <div class="container clearfix">
					<div class="heading-block center nobottomborder nobottommargin">
						<h2><font color="#40442b">"List Product"</font></h2>
					</div>
				</div>
                            </section>
		<div class="section-pr section-lg text-center" id="produk-terkait">
		<div class="container">
			<!--<h2 class="heading">LIST PRODUCTS</h2>-->
			<!--<hr class="bold"/><br/>-->
			<div class="row">			
			<div class="col-md-4" data-animate="bounceIn">			
					<div class="product-item">
						<a href="../products/glutacol-vc-cleansing-peel-gel">
						<img src="assets/images/products/cleanser1.png" alt="Glutacol VC Cleasing Peel Gel"></a>
						<div class="product-title">Glutacol VC Celansing Peel Gel</div>
					</div>
				</div>
				
			<div class="col-md-4" data-animate="bounceIn">
					<div class="product-item">
						<a href="../products/glutacol-vc-renewal-fluffy-mask">
						<img src="assets/images/products/Glutacol-VC-Renewal-Fluffy-Mask.png" alt="Glutacol VC Renewal Fluffy Mask"></a>
						<div class="product-title">Glutacol VC Renewal Fluffy Mask</div>
					</div>
				</div>
				
			<div class="col-md-4" data-animate="bounceIn">
					<div class="product-item">
						<a href="../products/glutacol-vc-body-moizturizer-foam">
						<img src="assets/images/products/body_care.png" alt="Glutacol VC Body Moisturizer Foam"></a>
						<div class="product-title">Glutacol VC Body Moisturizer Foam</div>
					</div>
				</div>				

			</div>
		</div>
	</div>
	
	</div>

<?php
include "../../footer.php";
?>