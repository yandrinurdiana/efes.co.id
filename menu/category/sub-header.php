<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="CIC-WebPro" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="../../assets/main.css" type="text/css" />
        	<link rel="stylesheet" href="../../assets/css/subscribe.css" type="text/css" />
	<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- <link rel="stylesheet" href="/../../assets/particles/css/style.css" type="text/css"/> -->
	<link rel="stylesheet" href="../../assets/css/swiper.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->

	<!-- Document Title
	============================================= -->
	<title>EFES NETWORK</title>
	<style type="text/css">
		.whatsapp-box{
        position: fixed;
        bottom:50px;
        right:15px;
        z-index: 99;
        background: #fff;
        width: 350px;
        border-radius: 10px;
        overflow: hidden;
        box-shadow: 0px 1px 10px rgba(0,0,0,0.1);
        transform: translateY(100%);
        opacity: 0;
        visibility: hidden;
        transition: .5s cubic-bezier(0.175, 0.885, 0.32, 1.275);
    }
    .whatsapp-box.active{
        transform: translateY(0);
        visibility: visible;
        opacity: 1;
    }
    .whatsapp-box .welcome-text svg{
        width: 20px;
        height: auto;
    }
    .whatsapp-box .welcome-text svg{
        height: 18px;
        width: auto;
        fill:#aaa;
        vertical-align: middle;
    }
    .whatsapp-box .welcome-text .time{
        font-size: 12px;
        color:#aaa;
    }
    .whatsapp-box .whatsapp-form svg{
        height: 20px;
        width: auto;
        fill:#aaa;
    }
    .whatsapp-box .heading{
        background:linear-gradient(#2ecc71,#1ABF60);
        padding:30px;
        color:#fff;
        text-align: center;
        position: relative;
    }
    .whatsapp-box .heading .btn-close{
        background: none;
        border:none;
        outline: none;
        box-shadow: none;
        position: absolute;
        top:0;
        left:0;
        color:#fff;
        padding:15px 30px;
        font-size: 20px;
    }
    .whatsapp-box .heading .avatar{
        width: 80px;
        height: 80px;
        background: #575d39;
        border-radius: 50%;
        overflow: hidden;
        margin:0 auto 10px;
    }
    .whatsapp-box .heading .avatar img{
        width: 100%;
        height: 100%;
        object-fit: cover;
    }
    .whatsapp-box .chatbox{
        padding:15px;
        background: #eee;
    }
    .whatsapp-box .whatsapp-form{
        display: flex;
        flex-direction: row;
        flex:1;
        padding:10px;
    }
    .whatsapp-box .whatsapp-form .form-control{
        width: 100%;
        border:none;
        box-shadow: none;
        background: none;
    }
    .whatsapp-box .whatsapp-form .btn-send{
        width: auto;
        background: none;
        border:none;
        outline: none;
    }
    .whatsapp-box .welcome-text{
        background: #E4FDC7;
        font-size: 14px;
        position: relative;
        padding:15px;
        box-shadow: 0px 1px 3px rgba(0,0,0,0.1);
        border-radius: 5px;
        font-family: "roboto", sans-serif;
        margin-left:15px;
        line-height: 120%;
        transform: translateY(50px);
        opacity: 0;
        visibility: hidden;
        transition: .3s ease;
        transition-delay: .6s;
    }
    .whatsapp-box.active .welcome-text{
        opacity: 1;
        visibility: visible;
        transform: translateY(0);
    }
    .whatsapp-box .welcome-text:before{
        content: "";
        position: absolute;
        top:0;
        left:-16px;
        border:8px solid transparent;
        border-right: 8px solid #E4FDC7;
        border-top: 8px solid #E4FDC7;
    }
    .whatsapp-box .welcome-text .notice{
        float: right;
        margin:5px 0 0;
    }
    @media (max-width: 767px){
        .welcome-tip{
            display: none;
        }
        .btn-whatsapp{
            width: auto;
            left:0;
            right:0;
            border-radius: 0;
        }
        .whatsapp-box{
            top:0;
            left:0;
            bottom:0;
            right:0;
            z-index: 99999;
            width: auto;
            background: #eee;
            border-radius: 0;
            transition: .5s ease;
        }
        .whatsapp-form{
            position: absolute;
            bottom:0;
            left:0;
            right:0;
            box-shadow: 0px -10px 50px -15px rgba(0,0,0,0.1);
            background: #fff;
        }
    }

    .wa_btn {
    background-color: green;
    margin: auto;
    padding: 10px 10px 10px;
    color: white;
    border-radius: 8px;



}
	</style>


</head>

<body class="stretched">

<?php
define('TIMEZONE', 'Asia/Jakarta');
?>
		<!-- Header
		============================================= -->
		<header id="header" class="transparent-header">
			<div id="header-wrap">
				<div class="container clearfix">
					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

					<!-- Logo
					============================================= -->
					<div id="logo">
						<a href="/"><img src="../../assets/images/logo.png" alt="efes Logo"></a>
					</div><!-- #logo end -->

<?php
include "navbar.php";
?>
				</div>
			</div>
		</header><!-- #header end -->