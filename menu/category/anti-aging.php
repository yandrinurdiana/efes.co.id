<?php
include "sub-header.php";
?>
		<!-- Page Title
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<h1>Anti Aging</h1>
				<span>PERAWATAN WAJAH</span>
				<ol class="breadcrumb">
					<li><a href="#">Product</a></li>
					<li class="active">Category</li>
					<li class="active">Anti Aging</li>
				</ol>
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

	<div class="text-center">
		<h2 class="heading"><span>PERAWATAN WAJAH</span></h2>
		<h5>KULIT WAJAH YANG SEHAT, BERSIH, CERAH DAN ALAMI BERAWAL DARI SINI</h5>
	</div>
	<br/><br/>

					<div class="col_half">
						<img src="../../assets/images/products/anti_aging.png" alt="Glutacol VC Renewal Lift"/>
					</div>

					<div class="col_half col_last">
						<h5>Glutacol VC Renewal Lift</h5>
						<!--Harga Jual : Rp. 1.740.000<br>-->
						<!--Harga Konsultan : Rp. 1.450.000<br><br>-->
						NA18162000237<br>
						40gr<br><br>
						EFES Glutacol VC Renewal Lift mengandung gluthatione, kolagen, dan probiotik, yang diolah dengan dengan proses enkapsulasi
						<br><br>

						<?php
                    $gid = '';
                    $gpaket = '';
                    if (isset($_GET['id'])) {
                        $gid = $_GET['id'];
                    }
                    $cs = [
                        ['name' => 'Meylisa', 'mobile' => '6281392507861', 'email' => 'cs5.heptaco@gmail.com'],
                       
                    ];
                    $keys = array_keys($cs);
                    $random = $keys[array_rand($keys,1)];
                    $name = $cs[$random]['name'];
                    $mobile = $cs[$random]['mobile'];
                    $email = $cs[$random]['email'];
                    ?>

						<a class="wa_btn btn"><span style="font-size: 20px;"><span style="font-size: 20px;"><i class="fa fa-whatsapp" style="font-size:20px"></i> <span style="font-size: 20px; font-weight: normal;">Order Via WA Sekarang!</span></a>
					</div>

					<!-- star new chat -->
                <div class="whatsapp-box">
    <div class="heading">
        <button class="btn-close" type="button"><i class="fa fa-angle-left"></i></button>
        <div class="avatar"><img src="../../assets/images/profile/profile.jpg" alt=""></div>
        <div class="name"><?php echo $name;?></div>
    </div>
    <div class="chatbox">
        <div class="welcome-text">
            Selamat <span class="greeting"></span>, saya <?php echo $name; ?> dari EFES. Silahkan chat saya untuk konsultasi atau order Paket Skincare EFES By dr. Fredi Setyawan!
            <div class="notice">
                <span class="time"></span>
                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="594.149px" height="594.149px" viewBox="0 0 594.149 594.149" style="enable-background:new 0 0 594.149 594.149;" xml:space="preserve">
                    <g>
                        <g id="done-all">
                        <path d="M448.8,161.925l-35.7-35.7l-160.65,160.65l35.7,35.7L448.8,161.925z M555.899,126.225l-267.75,270.3l-107.1-107.1l-35.7,35.7l142.8,142.8l306-306L555.899,126.225z M0,325.125l142.8,142.8l35.7-35.7l-142.8-142.8L0,325.125z"/></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g> 
                    </g>
                </svg>
            </div>
        </div>
        <br>
        <div class="welcome-text">
            Kami bisa COD, kirim barang terlebih dahulu dan bayar ditempat!
            <div class="notice">
                <span class="time"></span>
                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="594.149px" height="594.149px" viewBox="0 0 594.149 594.149" style="enable-background:new 0 0 594.149 594.149;" xml:space="preserve">
                    <g>
                        <g id="done-all">
                        <path d="M448.8,161.925l-35.7-35.7l-160.65,160.65l35.7,35.7L448.8,161.925z M555.899,126.225l-267.75,270.3l-107.1-107.1l-35.7,35.7l142.8,142.8l306-306L555.899,126.225z M0,325.125l142.8,142.8l35.7-35.7l-142.8-142.8L0,325.125z"/></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g> 
                    </g>
                </svg>
            </div>
        </div>
    </div>
    <form class="whatsapp-form" action="redirect.php?urls=<?php echo $urls ?>" data-phone="<?php echo $mobile; ?>">
        <input name="wa_message" type="text" class="form-control" placeholder="Reply.." value="Aku mau order Paket EFES Renewal Lift dong!" autocomplete="off">
        <button class="btn btn-send" type="submit">
            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="535.5px" height="535.5px" viewBox="0 0 535.5 535.5" style="enable-background:new 0 0 535.5 535.5;" xml:space="preserve"><g><g id="send"><polygon points="0,497.25 535.5,267.75 0,38.25 0,216.75 382.5,267.75 0,318.75"/></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
            </svg>
        </button>
    </form>
</div>
                <!-- end new chat -->

					<div class="clear"></div>

				</div>

			</div>

		</section><!-- #content end -->

<?php
include "footer.php";
?>
<script type="text/javascript">
    $(function(){
        
        $('.wa_btn').on('click',function(){
            if($('.whatsapp-box').hasClass('active')){
                $('.whatsapp-box').removeClass('active');
            } else {
                $('.whatsapp-box').addClass('active');
                greeting();

            }
        });
        function greeting(){
            var time = new Date();
            var timeText = time.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
            $('.whatsapp-box .time').text(timeText);
            setTimeout(function() { $('input[name="wa_message"]').focus() }, 1000);

            var curHr = time.getHours()

            if (curHr < 10) {
                var greeting = 'pagi';
            } else if (curHr < 15) {
                var greeting = 'siang';
            } else if (curHr < 18) {
                var greeting = 'sore';
            } else {
                var greeting = 'malam';
            }
            $('.whatsapp-box .greeting').text(greeting);
        }
        $('.whatsapp-box .btn-close').on('click',function(){
            $('.whatsapp-box').removeClass('active');
        });

        $('.whatsapp-form').on('submit',function(){
            var url;
            var phone = $(this).data('phone');
            var message = $('.whatsapp-form input[name=wa_message]').val();
            if($(window).width() > 768){
                // window.open("https://web.whatsapp.com/send?phone="+phone+"&text="+message+"");
                url = "https://web.whatsapp.com/send?phone="+phone+"&text="+message+"";
            } else {
                // window.open("https://wa.me/"+phone+"?text="+message+"");
                url = "https://wa.me/"+phone+"?text="+message+"";
            }
            window.open("redirect.php?urls="+url+"");
            return false;
        });
        var welcomeTip = localStorage.getItem("welcome-tip");
        if(welcomeTip != 'hidden'){
            $('.welcome-tip').addClass('active');
        } else {
            $('.welcome-tip').removeClass('active');
        }
        $('#welcome-tip-close').on('click',function(){
            localStorage.setItem("welcome-tip", "hidden");
            $('.welcome-tip').removeClass('active');
        });

        $(window).on('scroll',function(){
            if($(window).scrollTop() >  200){
                localStorage.setItem("welcome-tip", "hidden");
                $('.welcome-tip').removeClass('active');
            }
        });


    });
</script>	