<?php
include "sub-header.php";
?>
		<!-- Page Title
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<h1>About Us</h1>
				<span>Our Company Profile</span>
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li class="active">About Us</li>
				</ol>
			</div>

		</section><!-- #page-title end -->
		
		<!-- Portfolio Single Gallery
		============================================= -->
		<section id="slider" class="slider-parallax swiper_wrapper clearfix" style="height: 600px;">

			<div class="swiper-container swiper-parent">
				<div class="swiper-wrapper">
					<div class="swiper-slide" style="background-image: url('assets/images/about_slider.jpg');"></div>
				</div>
			</div>

		</section><!-- .portfolio-single-image end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				
				
				<div class="section dark nomargin noborder" style="height: 550px;">
					<div class="container vertical-middle center clearfix">
						<div class="emphasis-title nomargin" data-animate="fadeInUp">
							<h2 style="font-size: 56px;">EFES NETWORK</h2>
						</div>
					</div>
					<div class="video-wrap">
						<video poster="assets/images/videos/explore-poster.jpg" preload="auto" loop autoplay muted>
							<source src='images/videos/explore.mp4' type='video/mp4' />
							<source src='images/videos/explore.webm' type='video/webm' />
						</video>
						<div class="video-overlay"></div>
					</div>
				</div>

				<div class="section dark notopmargin nobottommargin noborder" style="padding: 20px 0;background-color: #222;">

				</div>

				<div class="col-md-12 nopadding common-height ohidden">
					<div class="col-md-6 col-padding" style="background: url('assets/images/company_office.jpg') center center no-repeat; background-size: cover;">
						<div>&nbsp;</div>
					</div>
					<div class="col-md-6 col-padding">

						<div class="clearfix">
							<h3>Selamat Datang di<br> PT. EFES NETWORK GLOBAL</h3>

							<div class="col-sm-9 nopadding topmargin-sm clearfix">

								<div class="feature-box fbox-outline fbox-light fbox-effect" data-animate="fadeIn">
									</div>
									<h3>Company Profile</h3>
									<p>Latar Belakang Perusahaan PT. Efes Global Network adalah sebuah perusahaan Network Marketing yang bergerak dibidang Kosmestik dan Peluang Bisnis. 
									Dengan konsep kemitraan sebagaimana tertuang didalam visi misinya. 
									Merupakan perusahaan perdana di Indonesia yang menggunakan Teknologi Modern yang sangat membantu dibidang kecantikan di Indonesia. 
									Setelah melalui proses sosialisasi yang cukup, produk EFES telah mendapat tempat di masyarakat karena manfaatnya yang dirasakan. 
									Dr Fredy Setyawan, DR Edwin Susanto, Ir. HM Nurcahyo Riswanto sebagai Team Founder dan didukung oleh Tim Manajemen yang berpengalaman sejak 1997 
									dan Sistem Bisnis Networking yang dirancang untuk mudah dijalankan dan mampu menciptakan para networker - networker yang sukses, 
									maka kami optimis akan berkembang besar bersama mitra.</p>
								</div>
							</div>
						</div>

					</div>
				</div>

				<div class="clear"></div>

				<div class="section nomargin">
					<div class="container clearfix">
						<div class="heading-block center nomargin">
							<h3>TEAM FOUNDER EFES NETWORK</h3>
						</div>
					</div>
				</div>
				
				<div class="row">
				<center>
					<a><img class="image_fade" src="assets/images/dr_fredy_setyawan.jpg" alt="dr fredy setyawan"></a>
					<a><img class="image_fade" src="assets/images/dr_Edwin_sutanto.jpg" alt="dr Edwin sutanto"></a>
					<a><img class="image_fade" src="assets/images/hm_nurcahyo_riswanto.jpg" alt="hm_nurcahyo_riswanto"></a>
				</center>
				</div>
				

				<div class="clear"></div>

				<div class="container topmargin-lg clearfix">

					<div class="col_one_third">
						<h4>Kantor Pusat</h4>
						PT. EFES GLOBAL NETWORK<br>
						Gedung Bank Natasha<br>
						Jl. Urip Sumoharjo. No. 65,<br>
						Yogyakarta<br>
						Indonesia<br>
					</div>

					<div class="col_one_third">
						<h4>Kantor Marketing</h4>
						Gedung Navagreen Lt. 4<br>
						Jl. Tanjung Duren Raya No. 137 a-c,<br> 
						Jakarta Barat<br>
						Indonesia<br>
					</div>
					
					<div class="col_three_third">
						<h4> Call Center</h4>
						(0274) 564251<br>
						customercare@efes.co.id<br> 
						Senin - Jumat: 08.00 - 16.00<br>
						Sabtu: 08.00 - 14.00<br>
						Minggu: Libur
					</div>



				</div>

			</div>

		</section><!-- #content end -->

<?php
include "footer.php";
?>