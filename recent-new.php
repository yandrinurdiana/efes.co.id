<?php
include "sub-header.php";
?>

            <!-- Portfolio Grid -->
                <sections class="bg-light" id="portfolio">
                  <div class="container">
                    <div class="row">
                      <div class="col-lg-12 text-center">
                        <h2 class="section-heading text-uppercase">-BOP-</h2>
                        <h3 class="section-subheading text-muted">(Businnnes Opportunity Presentation)</h3>
                      </div>
                    </div>
                      <center>
                    <div class="rows">
                        <div class="col-md-3 col-sm-6 portfolio-item"></div>
                        <!--<div class="col-md-3 col-sm-6 portfolio-item"></div>-->
                      <div class="col-md-3 col-sm-6 portfolio-item">
                        <a class="portfolio-link" data-toggle="modal" href="#portfolioModal1">
                          <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                              <i class="fa fa-plus fa-3x"></i>
                            </div>
                          </div>
                            <img  src="assets/images/bopyk_kcl.jpg" alt="bop"> 
                        </a>
                        <div class="portfolio-caption">
                          <h4>Setiap Rabu & Jum'at</h4>
                          <!--<p class="text-muted">In</p>-->
                          <p class="text-muted">Gedung Bank Natasha, Lt. 3</p>
                          <p class="text-muted">Jl. Urip Sumoharjo. No. 65, YOGYAKARTA</p>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-6 portfolio-item">
                        <a class="portfolio-link" data-toggle="modal" href="#portfolioModal2">
                          <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                              <i class="fa fa-plus fa-3x"></i>
                            </div>
                          </div>
                            <img src="assets/images/bopjkt_kcl.jpg" alt=""> 
                        </a>
                        <div class="portfolio-caption">
                          <h4>Setiap Selasa & Kamis</h4>
                          <p class="text-muted">Gedung Navagreen Lt. 4</p>
                          <p class="text-muted">Jl. Tanjung Duren Raya No. 137 a-c, JAKARTA</p>
                        </div>
                      </div>
                    </center>
<!--                      <div class="col-md-4 col-sm-6 portfolio-item">
                        <a class="portfolio-link" data-toggle="modal" href="#portfolioModal3">
                          <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                              <i class="fa fa-plus fa-3x"></i>
                            </div>
                          </div>
                          <img class="img-fluid" src="img/portfolio/03-thumbnail.jpg" alt="">
                        </a>
                        <div class="portfolio-caption">
                          <h4>Finish</h4>
                          <p class="text-muted">Identity</p>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-6 portfolio-item">
                        <a class="portfolio-link" data-toggle="modal" href="#portfolioModal4">
                          <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                              <i class="fa fa-plus fa-3x"></i>
                            </div>
                          </div>
                          <img class="img-fluid" src="img/portfolio/04-thumbnail.jpg" alt="">
                        </a>
                        <div class="portfolio-caption">
                          <h4>Lines</h4>
                          <p class="text-muted">Branding</p>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-6 portfolio-item">
                        <a class="portfolio-link" data-toggle="modal" href="#portfolioModal5">
                          <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                              <i class="fa fa-plus fa-3x"></i>
                            </div>
                          </div>
                          <img class="img-fluid" src="img/portfolio/05-thumbnail.jpg" alt="">
                        </a>
                        <div class="portfolio-caption">
                          <h4>Southwest</h4>
                          <p class="text-muted">Website Design</p>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-6 portfolio-item">
                        <a class="portfolio-link" data-toggle="modal" href="#portfolioModal6">
                          <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                              <i class="fa fa-plus fa-3x"></i>
                            </div>
                          </div>
                          <img class="img-fluid" src="img/portfolio/06-thumbnail.jpg" alt="">
                        </a>
                        <div class="portfolio-caption">
                          <h4>Window</h4>
                          <p class="text-muted">Photography</p>
                        </div>
                      </div>-->
                    </div>
                  </div>
                </sections>

            <!-- Portfolio Modals -->

    <!-- Modal 1 -->
    <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="rows">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                  <!-- Project Details Go Here -->
                  <h2 class="text-uppercase">-BOP-</h2>
                  <p class="item-intro text-muted">Businnes Opportunity Presentation</p>
                  <p class="item-intro text-muted">YOGYAKARTA</p>
                  <img class="img-fluid d-block mx-auto" src="assets/images/bopyk.jpg" alt="">
                  <p>Mengundang Anda dalam Acara BOP (Businnes Opportunity Presentation) tentang "REVOLUSI DUNIA KECANTIKAN & PELUANG USAHA BARU". Teknologi Nanosome dan Enkapsulasi Terbukti ALAMI Merawat Kelembutan dan Mencerahkan Kulit Tanpa Iritasi. [Reg. BPOM RI, MUI, dan GMP]</p>
                  <ul class="list-inline1">
                    <li>Setiap Hari Rabu</li>
                    <li>Jam : 14.00 - Selesai</li>
                    <li>&amp;</li>
                    <li>Setiap Hari Jum'at</li>
                    <li>Jam : 15.30 - Selesai</li>
                    <!--<li>Category: Illustration</li>-->
                  </ul>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Close</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Modal 2 -->
    <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="rows">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                   <!--Project Details Go Here--> 
                  <h2 class="text-uppercase">-BOP-</h2>
                  <p class="item-intro text-muted">(Businnes Opportunity Presentation)</p>
                  <p class="item-intro text-muted">JAKARTA</p>
                  <img class="img-fluid d-block mx-auto" src="assets/images/bopjkt.jpg" alt="">
                  <p>Mengundang Anda dalam Acara BOP (Businnes Opportunity Presentation) tentang "REVOLUSI DUNIA KECANTIKAN & PELUANG USAHA BARU". Teknologi Nanosome dan Enkapsulasi Terbukti ALAMI Merawat Kelembutan dan Mencerahkan Kulit Tanpa Iritasi. [Reg. BPOM RI, MUI, dan GMP]</p>
                  <ul class="list-inline1">
                    <li>Setiap Hari Selasa dan Kamis</li>
                    <li>Jam     : 14.00 - Selesai</li>
                    <!--<li>Category: Graphic Design</li>-->
                  </ul>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Close</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

<!--     Modal 3 
    <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                   Project Details Go Here 
                  <h2 class="text-uppercase">Project Name</h2>
                  <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                  <img class="img-fluid d-block mx-auto" src="img/portfolio/03-full.jpg" alt="">
                  <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                  <ul class="list-inline">
                    <li>Date: January 2017</li>
                    <li>Client: Finish</li>
                    <li>Category: Identity</li>
                  </ul>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Close Project</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

     Modal 4 
    <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                   Project Details Go Here 
                  <h2 class="text-uppercase">Project Name</h2>
                  <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                  <img class="img-fluid d-block mx-auto" src="img/portfolio/04-full.jpg" alt="">
                  <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                  <ul class="list-inline">
                    <li>Date: January 2017</li>
                    <li>Client: Lines</li>
                    <li>Category: Branding</li>
                  </ul>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Close Project</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

     Modal 5 
    <div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                   Project Details Go Here 
                  <h2 class="text-uppercase">Project Name</h2>
                  <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                  <img class="img-fluid d-block mx-auto" src="img/portfolio/05-full.jpg" alt="">
                  <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                  <ul class="list-inline">
                    <li>Date: January 2017</li>
                    <li>Client: Southwest</li>
                    <li>Category: Website Design</li>
                  </ul>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Close Project</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

     Modal 6 
    <div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="close-modal" data-dismiss="modal">
            <div class="lr">
              <div class="rl"></div>
            </div>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto">
                <div class="modal-body">
                   Project Details Go Here 
                  <h2 class="text-uppercase">Project Name</h2>
                  <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                  <img class="img-fluid d-block mx-auto" src="img/portfolio/06-full.jpg" alt="">
                  <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                  <ul class="list-inline">
                    <li>Date: January 2017</li>
                    <li>Client: Window</li>
                    <li>Category: Photography</li>
                  </ul>
                  <button class="btn btn-primary" data-dismiss="modal" type="button">
                    <i class="fa fa-times"></i>
                    Close Project</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>-->
<!--		 Page Title
		============================================= 
		<section id="page-title">

			<div class="container clearfix">
				<h1>Testimoni Product Efes</h1>
				<span>Our Gallery Event</span>
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li class="active">Testimoni</li>
				</ol>
			</div>

		</section> #page-title end 

		 Content
		============================================= 
		<section id="content">

			<div class="content-wrap nobottompadding">
                        <div class="row">
                               <div class="gallery col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                   <h1 class="gallery-title" align="center">Gallery</h1>
                               </div>
				<div class="container clearfix">
                                        <h1>Testimoni Product Efes</h1>
					 Gallery Filter
					============================================= 
					<ul id="portfolio-filter" class="portfolio-filter clearfix" data-container="#portfolio">

						<li class="activeFilter"><a href="#" data-filter="*">Show All</a></li>
						<li><a href="#" data-filter=".pf-icons">Brightening</a></li>
						<li><a href="#" data-filter=".pf-illustrations">Renewal Lift</a></li>
						<li><a href="#" data-filter=".pf-uielements">Fluffy Mask</a></li>

					</ul> #portfolio-filter end 

					<div id="portfolio-shuffle" class="portfolio-shuffle" data-container="#portfolio">
						<i class="icon-random"></i>
					</div>

				</div>

				 Gallery Items
				============================================= 
				<div id="portfolio" class="portfolio grid-container portfolio-nomargin portfolio-notitle portfolio-full clearfix">

					<article class="col-md-3 portfolio-item pf-media pf-icons" border="1">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/renewallift.png" alt="Stockist">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/renewallift.png" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>
							</div>
						</div>
						<div class="portfolio-desc">
							<h3><a href="#">Renewal Lift</a></h3>
							<span><a href="#">Pemakaian Selama 2 Minggu</a></span>
						</div>
                                        </article>
                                    <article class="col-md-3 portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/stockist.jpg" alt="Stockist">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/stockist.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>
							</div>
						</div>
						<div class="portfolio-desc">
							<h3><a href="#">Stockist</a></h3>
							<span><a href="#">Category 1</a></span>
						</div>
					</article>

					<article class="col-md-3 portfolio-item pf-illustrations">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/office.jpg" alt="Office">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/office.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>
							</div>
						</div>
						<div class="portfolio-desc">
							<h3><a href="#">Factory</a></h3>
							<span><a href="#">Category 2</a></span>
						</div>
					</article>

					<article class="col-md-3 portfolio-item pf-graphics pf-uielements">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bank_bpr_natasha.jpg" alt="bank_bpr_natasha">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/bank_bpr_natasha.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>
							</div>
						</div>
						<div class="portfolio-desc">
							<h3><a href="#">bank_bpr_natasha</a></h3>
							<span><a href="#">Category 3</a></span>
						</div>
					</article>

					<article class="col-md-3 portfolio-item pf-icons pf-illustrations">
						<div class="portfolio-image">
							<div class="fslider" data-arrows="false" data-speed="400" data-pause="4000">
								<div class="flexslider">
									<div class="slider-wrap">
										<div class="slide"><a href="#"><img src="assets/images/gallery/mini/meeting1.jpg" alt="meeting"></a></div>
										<div class="slide"><a href="#"><img src="assets/images/portfolio/4/4-1.jpg" alt="Morning Dew"></a></div>
									</div>
								</div>
							</div>
							<div class="portfolio-overlay" data-lightbox="gallery">
								<a href="assets/images/gallery/meeting1.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
								<a href="assets/images/gallery/photosoon.jpg" class="hidden" data-lightbox="gallery-item"></a>
								<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>
							</div>
						</div>
						<div class="portfolio-desc">
							<h3><a href="portfolio-single-gallery.html">Meeting Staff</a></h3>
							<span><a href="#">Category 2</a></span>
						</div>
					</article>

					<article class="col-md-3 portfolio-item pf-uielements pf-media">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/blessing.jpg" alt="Blessing">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/blessing.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>
							</div>
						</div>
						<div class="portfolio-desc">
							<h3><a href="#">Blessing</a></h3>
							<span><a href="#">Category 3</a></span>
						</div>
					</article>

					<article class="col-md-3 portfolio-item pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="/assets/images/gallery/mini/all_staff.jpg" alt="All Staff">
							</a>
							<div class="portfolio-overlay">
								<a href="/assets/images/gallery/all_staff.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>
							</div>
						</div>
						<div class="portfolio-desc">
							<h3><a href="#">All Staff</a></h3>
							<span><a href="#">Category 1</a></span>
						</div>
					</article>
					

					<article class="col-md-3 portfolio-item pf-uielements pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/photosoon.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
							 data-lightbox="iframe" 
								<a href="assets/images/gallery/photosoon.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-play"></i></a>
								<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>
							</div>
						</div>
						<div class="portfolio-desc">
							<h3><a href="#">Photo Unavailable</a></h3>
							<span><a href="#">Category 1</a></span>
						</div>
					</article>

					<article class="col-md-3 portfolio-item pf-illustrations pf-icons">
						<div class="portfolio-image">
							<div class="fslider" data-arrows="false" data-speed="650" data-pause="3500" data-animation="fade">
								<div class="flexslider">
									<div class="slider-wrap">
										<div class="slide"><a href="#"><img src="assets/images/gallery/photosoon.jpg" alt="Coming Soon.."></a></div>
										<div class="slide"><a href="#"><img src="assets/images/gallery/photosoon.jpg" alt="Coming Soon.."></a></div>
										<div class="slide"><a href="#"><img src="assets/images/gallery/photosoon.jpg" alt="Coming Soon.."></a></div>
									</div>
								</div>
							</div>
							<div class="portfolio-overlay" data-lightbox="gallery">
								<a href="assets/images/gallery/photosoon.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
								<a href="assets/images/gallery/photosoon.jpg" class="hidden" data-lightbox="gallery-item"></a>
								<a href="assets/images/gallery/photosoon.jpg" class="hidden" data-lightbox="gallery-item"></a>
								<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>
							</div>
						</div>
						<div class="portfolio-desc">
							<h3><a href="#">Photo Unavailable</a></h3>
							<span><a href="#">Category 2</a></span>
						</div>
					</article>

					<article class="col-md-3 portfolio-item pf-graphics pf-media pf-uielements">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/photosoon.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
							 data-lightbox="iframe" 
								<a href="assets/images/gallery/photosoon.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-play"></i></a>
								<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>
							</div>
						</div>
						<div class="portfolio-desc">
							<h3><a href="#">Photo Unavailable</a></h3>
							<span><a href="#">Category 2</a></span>
						</div>
					</article>

					<article class="col-md-3 portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/photosoon.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/photosoon.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>
							</div>
						</div>
						<div class="portfolio-desc">
							<h3><a href="#">Photo Unavailable</a></h3>
							<span><a href="#">Category 3</a></span>
						</div>
					</article>

					<article class="col-md-3 portfolio-item pf-illustrations pf-graphics">
						<div class="portfolio-image">
							<div class="fslider" data-arrows="false" data-speed="700" data-pause="7000">
								<div class="flexslider">
									<div class="slider-wrap">
										<div class="slide"><a href="#"><img src="assets/images/gallery/photosoon.jpg" alt="Coming Soon.."></a></div>
										<div class="slide"><a href="#"><img src="assets/images/gallery/photosoon.jpg" alt="Coming Soon.."></a></div>
									</div>
								</div>
							</div>
							<div class="portfolio-overlay" data-lightbox="gallery">
								<a href="assets/images/gallery/photosoon.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
								<a href="assets/images/gallery/photosoon.jpg" class="hidden" data-lightbox="gallery-item"></a>
								<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>
							</div>
						</div>
						<div class="portfolio-desc">
							<h3><a href="#">Photo Unavailable</a></h3>
							<span><a href="#">Category 3</a></span>
						</div>
					</article>

				</div> #portfolio end 

			</div>
                        </div>
		</section> #content end -->

<?php
include "footer.php";
?>