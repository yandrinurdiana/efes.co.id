<?php
include "sub-header.php";
?>
		<!-- Page Title
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<h1>Testimonials</h1>
				<span></span>
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li class="active">Testimoni</li>
				</ol>
			</div>

		</section><!-- #page-title end -->
		
		<!-- Portfolio Single Gallery
		============================================= -->
		<section class="swiper_wrapper clearfix" style="height: 330px;">

			<div class="swiper-container swiper-parent">
				<div class="swiper-wrapper">
					<div class="swiper-slide" style="background-image: url('assets/images/testi-header.jpg');"></div>
				</div>
			</div>

		</section><!-- .portfolio-single-image end -->

		<!-- Content
		============================================= -->
		
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">
				
							<header class="page-header pt-style-1 ">
            <h1 class="page-title">
			<span>TESTIMONIAL</span></h1> 
                            <p class="lead">
                                Below contains a collection of testimonials from our various cutomers.
                            </p>                           
<div class='col-md-12 testimonials'> 
<h1 class='topheader'><spans>Renewal Lift<spans></h1> 
<br>
<div class="row">
<div class="col-md-6">
<h3 class="videoheader"></h3>
<h4 class="compheader"></h4>
<div class="videoWrapper">
<a><img class="image_fade" src="assets/images/testimonials/testi1.jpeg"></a>
</div>

</div>
<div class="col-md-6">
<h3 class="videoheader"></h3>
<h4 class="compheader"></h4>
<div class="videoWrapper">
<a><img class="image_fade" src="assets/images/testimonials/testi2.jpeg"></a>
</div>
</div>
</div>

<p><hr class="clear spacer"/></p>
<div class="row">
<div class="col-md-6  col-md-offset-3">
<h3 class="videoheader"></h3>
<h4 class="compheader"></h4>
<div class="videoWrapper">
<a><img class="image_fade" src="assets/images/testimonials/testi3.jpeg"></a>
</div>
</div>
</div>    
 <!--<p><hr class="clear spacer"/></p>--> 
</div>        

</header><!-- .page-header -->
		
<!--                        <header class="page-header pt-style-1 ">
            
			<span></span></h1> 
                            <p class="lead">
<center>             -->
<!--<div class='col-md-12 testimonials'> 

<h1 class='topheader'>Demo Video 02 Bubble </h1>
<div class="row">
<div class="col-md-12">
<h3 class="videoheader"></h3>
<h4 class="compheader"></h4>
<div class="videoWrapper">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/lqCLhl5q-js" frameborder="0" allowfullscreen></iframe>
<iframe width="560" height="315" src="https://www.youtube.com/embed/2CUKqBAL-_g?rel=0" frameborder="0" allowfullscreen>
</iframe>
    </div> 
</div>
</div>  
-->
</div>   
                                
<!--		</header> .page-header 

					<div class="col_half">
					</div>

				<div class="clear"></div>

			</div>-->

		</section><!-- #content end -->

<?php
include "footer.php";
?>