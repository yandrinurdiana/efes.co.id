<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
            </div>
            <div class="modal-body">
               <img src="assets/images/popup_efes.jpg"/>
            </div>            
        </div>

    </div>
</div>

	<!-- Document Wrapper
	============================================= -->
	<section class="swiper_wrapper clearfix" style="height: 600px;">

			<div class="swiper-container swiper-parent">
				<div class="swiper-wrapper">
					<div class="swiper-slide" style="background-image: url('assets/images/parallax/home/9.jpg');"></div>
				</div>
			</div>

		</section>
		

		<!-- Content
		============================================= -->
		<section id="content">
			<div class="content-wrap nopadding">
				<div class="section parallax full-screen nomargin noborder" data-stellar-background-ratio="0.4">					
					<div class="vertical-middle">
						<div class="container clearfix">			
							<div class="row">
								<div class="col-lg-6 bottommargin-sm center" data-animate="bounceIn">
									<div class="counter counter-large counter-lined"> <img src="assets/images/parallax/beauty_story1.jpg"> </div>
									<h5><font color="#31351f"><b>Smooth face with a mask cleanser</b></font></h5>
								</div>

								<div class="col-lg-6 bottommargin-sm center" data-animate="bounceIn">
									<div class="counter counter-large counter-lined"> <img src="assets/images/parallax/beauty_story2.jpg"> </div>
									 <h5><font color="#40442b"><b>Ready Tips For You</b></font></h5>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="clear"></div>
				<div class="section nomargin noborder" style="background-color:#e1e1e1;text-shadow: 1px 1px #aeaeae;">
					<div class="heading-block center nobottomborder nobottommargin">
						<h2><font color="#40442b">"The Luxurious Natural Beauty Product"</font></h2>
					</div>
				</div>
				
                            <div class="col-md-12 nopadding common-height">
                                <div class="section nomargin noborder" style="background-image: url('assets/images/nodes.jpg');" data-stellar-background-ratio="0.4">					

					<div class="col-md-4 dark col-padding ohidden" data-animate="bounceIn">
					<center>
						<div>
							<a href="menu/category/mask.html" style="color: white"><h4 class="uppercase" style="font-weight: 600;">Glutacol VC Renewal<br> Fluffy Mask</h4>
							<p style="line-height: 1.8;"> <img src="assets/images/products/top_product/product1.png"> </p>
							<br>
						</div>
                                        </center>
					</div>
					
					<div class="col-md-4 dark col-padding ohidden" data-animate="bounceIn">
						<div>
						<center>
							<a href="menu/category/anti-aging.html" style="color: white"><h4 class="uppercase" style="font-weight: 600;">Glutacol VC<br> Renewal Lift</h4>							
							<p style="line-height: 1.8;"> <img src="assets/images/products/top_product/product4.png"> </p>
							<br>
						</div>
						</center>
					</div>				
					
					<div class="col-md-4 dark col-padding ohidden" data-animate="bounceIn">
						<div>
						<center>
							<a href="menu/category/brightening.html" style="color: white"><h4 class="uppercase" style="font-weight: 600;">Glutacol VC Brightening<br> Moisturizer</h4>							
							<p style="line-height: 1.8;"> <img src="assets/images/products/top_product/product6.png"> </p>
							<br>
						</div>
						</center>
					</div>
					<!--</div>-->
					<!--<div class="clear"></div>-->
				<!--</div>-->
					
				
					<div class="col-md-4 dark col-padding ohidden" data-animate="bounceIn">
						<div>
						<center>
							<a href="menu/category/o2buble.html" style="color: white"><h4 class="uppercase" style="font-weight: 600;">Glutacol VC O2 Bubble<br> Facial Cleanser</h4>							
							<p style="line-height: 1.8;"> <img src="assets/images/products/top_product/product2.png"> </p>
							<br>
						</div>
						</center>
					</div>

					<div class="col-md-4 dark col-padding ohidden" data-animate="bounceIn">
						<div>
						<center>
							<a href="menu/category/cleanser.html" style="color: white"><h4 class="uppercase" style="font-weight: 600;">Glutacol VC Cleansing<br> Peel Gel</h4>							
							<p style="line-height: 1.8;"> <img src="assets/images/products/top_product/product3.png"> </p>
							
						</div>
						</center>
					</div>
					
					<div class="col-md-4 dark col-padding ohidden" data-animate="bounceIn">
						<div>
						<center>
							<a href="menu/category/body-care.html" style="color: white"><h4 class="uppercase" style="font-weight: 600;">Glutacol VC Body<br> Moisturizer Foam</h4>							
							<p style="line-height: 1.8;"> <img src="assets/images/products/top_product/product5.png"> </p>
							<br>
						</div>
						</center>
					</div>
<!--				</div>
					<div class="clear"></div>
				</div>-->

			</div>

		</section><!-- #content end -->