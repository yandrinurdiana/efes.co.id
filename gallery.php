<?php
include "sub-header.php";
?>
		<!-- Page Title
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<h1>Gallery</h1>
				<span>Our Gallery Event</span>
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li class="active">Gallery</li>
				</ol>
			</div>

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap nobottompadding">

				<div class="container clearfix">

					<!-- Gallery Filter
					============================================= -->
					<ul id="portfolio-filter" class="portfolio-filter clearfix" data-container="#portfolio">

						<li><a href="#" data-filter="*">Show All</a></li>
						<!--<li><a href="#" data-filter=".pf-illustrations">BOP Office Yogyakarta</a></li>-->
                                                <li><a href="#" data-filter=".pf-icons">BOP Yogyakarta</a></li>
						<li><a href="#" data-filter=".pf-uielements">BOP Salatiga</a></li>
						<li class="activeFilter"><a href="#" data-filter=".pf-smrg">BOP Semarang</a></li>
						<li><a href="#" data-filter=".pf-jkt">BOP Jakarta</a></li>

					</ul><!-- #portfolio-filter end -->

					<div id="portfolio-shuffle" class="portfolio-shuffle" data-container="#portfolio">
						<i class="icon-random"></i>
					</div>

				</div>

				<!-- Gallery Items
				============================================= -->
                                
				<div id="portfolio" class="portfolio grid-container portfolio-nomargin portfolio-notitle portfolio-full clearfix">
                            <!--BOP ASTON YOGYAKARTA-->
                                        <article class="col-md-3 portfolio-item pf-media pf-uielements">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_sltg/1sltg.jpg" alt="Stockist">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/1sltg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Pembukaan Acara BOP oleh Ibu Direktur Rosyani S.H</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>

					<article class="col-md-3 portfolio-item pf-uielements">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_sltg/2sltg.jpg" alt="Office">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/2sltg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Antusias Calon Konsultan Efes</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>

					<article class="col-md-3 portfolio-item pf-graphics pf-uielements">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_sltg/3sltg.jpg" alt="re">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/3sltg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Antusias Calon Konsultan Efes</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-uielements">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_sltg/4sltg.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
							<!-- data-lightbox="iframe" -->
								<a href="assets/images/gallery/4sltg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-play"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Calon Konsultan EFES Fokus Saat Pembukaan Acara BOP</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
                                        <article class="col-md-3 portfolio-item pf-graphics pf-media pf-uielements">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_sltg/5sltg.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
							<!-- data-lightbox="iframe" -->
								<a href="assets/images/gallery/5sltg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-play"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Penjelasan Product Knowledge Oleh dr. Narto</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
<!--                                                    <div class="flexslider">
                                                     <div class="slider-wrap">-->
							<a href="#">
								<img src="assets/images/gallery/mini/bop_aston/1.jpeg" alt="Stockist">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/1.jpeg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
<!--                                                     </div>
                                                    </div>-->
						</div>
<!--						<div class="portfolio-desc">
							<h3>Antusias Calon Konsultan Efes</h3>
							<span>BOP di Grand Aston Yogyakarta</span>
						</div>-->
					</article>

					<article class="col-md-3 portfolio-item pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_aston/2.jpeg" alt="Office">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/2.jpeg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Ramainya Grand BOP EFES</h3>
							<span>BOP di Grand Aston Yogyakarta</span>
						</div>-->
					</article>

					
<!--					<article class="col-md-3 portfolio-item pf-icons pf-illustrations">
						<div class="portfolio-image">
							<div class="fslider" data-arrows="false" data-speed="400" data-pause="4000">
								<div class="flexslider">
									<div class="slider-wrap">
										<div class="slide"><a href="#"><img src="assets/images/gallery/mini/meeting1.jpg" alt="meeting"></a></div>
										<div class="slide"><a href="#"><img src="assets/images/portfolio/4/4-1.jpg" alt="Morning Dew"></a></div>
									</div>
								</div>
							</div>
							<div class="portfolio-overlay" data-lightbox="gallery">
								<a href="assets/images/gallery/meeting1.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
								<a href="assets/images/gallery/photosoon.jpg" class="hidden" data-lightbox="gallery-item"></a>
								<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>
							</div>
						</div>
						<div class="portfolio-desc">
							<h3><a href="portfolio-single-gallery.html">Meeting Staff</a></h3>
							<span><a href="#">Category 2</a></span>
						</div>
					</article>-->
<!--

					<article class="col-md-3 portfolio-item pf-uielements pf-media">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/blessing.jpg" alt="Blessing">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/blessing.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>
							</div>
						</div>
						<div class="portfolio-desc">
							<h3><a href="#">Blessing</a></h3>
							<span><a href="#">Category 3</a></span>
						</div>
					</article>

					<article class="col-md-3 portfolio-item pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="/assets/images/gallery/mini/all_staff.jpg" alt="All Staff">
							</a>
							<div class="portfolio-overlay">
								<a href="/assets/images/gallery/all_staff.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>
							</div>
						</div>
						<div class="portfolio-desc">
							<h3><a href="#">All Staff</a></h3>
							<span><a href="#">Category 1</a></span>
						</div>
					</article>
					-->

					<article class="col-md-3 portfolio-item pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_aston/4.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
							<!-- data-lightbox="iframe" -->
								<a href="assets/images/gallery/4.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-play"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Penyampaian Product Knowledge Oleh Bapak dr. Fredy Setyawan</h3>
							<span>BOP di Grand Aston Yogyakarta</span>
						</div>-->
					</article>

<!--					<article class="col-md-3 portfolio-item pf-illustrations pf-icons">
						<div class="portfolio-image">
							<div class="fslider" data-arrows="false" data-speed="650" data-pause="3500" data-animation="fade">
								<div class="flexslider">
									<div class="slider-wrap">
										<div class="slide"><a href="#"><img src="assets/images/gallery/photosoon.jpg" alt="Coming Soon.."></a></div>
										<div class="slide"><a href="#"><img src="assets/images/gallery/photosoon.jpg" alt="Coming Soon.."></a></div>
										<div class="slide"><a href="#"><img src="assets/images/gallery/photosoon.jpg" alt="Coming Soon.."></a></div>
									</div>
								</div>
							</div>
							<div class="portfolio-overlay" data-lightbox="gallery">
								<a href="assets/images/gallery/photosoon.jpg" class="left-icon" data-lightbox="gallery-item"><i class="icon-line-stack-2"></i></a>
								<a href="assets/images/gallery/photosoon.jpg" class="hidden" data-lightbox="gallery-item"></a>
								<a href="assets/images/gallery/photosoon.jpg" class="hidden" data-lightbox="gallery-item"></a>
								<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>
							</div>
						</div>
						<div class="portfolio-desc">
							<h3><a href="#">Photo Unavailable</a></h3>
							<span><a href="#">Category 2</a></span>
						</div>
					</article>-->

x
                                        <article class="col-md-3 portfolio-item pf-media pf-smrg">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_smrg/1smrg.jpg" alt="Stockist">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/1smrg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Pembukaan Acara BOP oleh Ibu Direktur Rosyani S.H</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>

					<article class="col-md-3 portfolio-item pf-smrg">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_smrg/2smrg.jpg" alt="Office">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/2smrg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Antusias Calon Konsultan Efes</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>

					<article class="col-md-3 portfolio-item pf-graphics pf-smrg">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_smrg/3smrg.jpg" alt="re">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/3smrg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Antusias Calon Konsultan Efes</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-smrg">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_smrg/4smrg.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
							<!-- data-lightbox="iframe" -->
								<a href="assets/images/gallery/4smrg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-play"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Calon Konsultan EFES Fokus Saat Pembukaan Acara BOP</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
                                        <article class="col-md-3 portfolio-item pf-graphics pf-media pf-smrg">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_smrg/5smrg.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
							<!-- data-lightbox="iframe" -->
								<a href="assets/images/gallery/5smrg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-play"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Penjelasan Product Knowledge Oleh dr. Narto</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_aston/6.jpeg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/6.jpeg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Fokus Calon Konsultan Pada Penjelasan Product Knowledge</h3>
							<span>BOP di Grand Aston Yogyakarta</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_aston/7.JPG" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/7.JPG" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Antusias Calon Konsultan Efes</h3>
							<span>BOP di Grand Aston Yogyakarta</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_aston/8.JPG" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/8.JPG" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Pembagian Doorprize Ke Calon Konsultan Yang Bisa Jawab Pertanyaan</h3>
							<span>BOP di Grand Aston Yogyakarta</span>
						</div>-->
					</article>
                                        <article class="col-md-3 portfolio-item pf-graphics pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_aston/13.jpg" alt="re">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/13.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Demo Product Efes Ke Calon Konsultan</h3>
							<span>BOP di Grand Aston Yogyakarta</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_aston/9.JPG" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/9.JPG" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Registrasi Kehadiran Untuk Acara Grand BOP EFES</h3>
							<span>BOP di Grand Aston Yogyakarta</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_aston/10.jpeg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/10.jpeg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Antusias Calon Konsultan Tentang Product EFES</h3>
							<span>BOP di Grand Aston Yogyakarta</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_aston/11.jpeg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/11.jpeg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Kasir Untuk Pembelian Product EFES</h3>
							<span>BOP di Grand Aston Yogyakarta</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_aston/12.JPG" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/12.JPG" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Penjelasan Cara Pakai Product EFES</h3>
							<span>BOP di Grand Aston Yogyakarta</span>
						</div>-->
					</article>
                            <!--BOP YOGYAKARTA-->
					<article class="col-md-3 portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_yk/1yk.jpg" alt="Stockist">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/1yk.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Antusias Calon Konsultan EFES</h3>
							<span>BOP di EFES Yogyakarta</span>
						</div>-->
					</article>

					<article class="col-md-3 portfolio-item pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_yk/2yk.jpg" alt="Office">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/2yk.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Penjelasan Product Knowledge oleh dr. Luci Kuswibawati</h3>
							<span>BOP di EFES Yogyakarta</span>
						</div>-->
					</article>

					<article class="col-md-3 portfolio-item pf-graphics pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_yk/3yk.jpg" alt="re">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/3yk.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Fokus Calon Konsultan dalam Penjelasan Product Knowledge</h3>
							<span>BOP di EFES Yogyakarta</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_yk/4yk.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
							<!-- data-lightbox="iframe" -->
								<a href="assets/images/gallery/4yk.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-play"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Presentasi Bersama Calon Konsultan EFES</h3>
							<span>BOP di EFES Yogyakarta</span>
						</div>-->
					</article>
                                        <article class="col-md-3 portfolio-item pf-graphics pf-media pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_yk/5yk.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
							<!-- data-lightbox="iframe" -->
								<a href="assets/images/gallery/5yk.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-play"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Fokus Calon Konsultan dalam Penjelasan Product Knowledge</h3>
							<span>BOP di EFES Yogyakarta</span>
						</div>-->
					</article>
                                           <article class="col-md-3 portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_yk/6yk.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/6yk.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Antusias Calon Konsultan Efes</h3>
							<span>BOP di EFES Yogyakarta</span>
						</div>-->
					</article>
					
					<article class="col-md-3 portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_yk/7yk.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/7yk.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Penyampaian Product Knowledge Oleh Konsultan EFES</h3>
							<span>BOP di EFES Yogyakarta</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_yk/8yk.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/8yk.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Calon Konsultan EFES Fokus Pada Penjelasan Product Knowledge</h3>
							<span>BOP di EFES Yogyakarta</span>
						</div>-->
					</article>
                            
					<article class="col-md-3 portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_yk/9yk.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/9yk.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Penjelasan Marketing Plan Oleh Konsultan EFES</h3>
							<span>BOP di EFES Yogyakarta</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_yk/10yk.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/10yk.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Penjelasan Product Knowledge oleh dr. Luci Kuswibawati</h3>
							<span>BOP di EFES Yogyakarta</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_yk/11yk.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/11yk.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Foto Bersama Dengan Simbol 3 Jari [EFES]</h3>
							<span>BOP di EFES Yogyakarta</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-icons">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_yk/12yk.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/12yk.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Foto Bersama Konsultan EFES</h3>
							<span>BOP di EFES Yogyakarta</span>
						</div>-->
					</article>
                            <!--BOP SALATIGA-->
					
                                           <article class="col-md-3 portfolio-item pf-media pf-uielements">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_sltg/6sltg.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/6sltg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Fokus Calon Konsultan Pada Penjelasa Product Knowledge</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
					
					<article class="col-md-3 portfolio-item pf-media pf-uielements">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_sltg/7sltg.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/7sltg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Ramainya Tamu Undangan Yang Antusias Hadir</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-uielements">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_sltg/8sltg.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/8sltg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Antusias Calon Konsultan Efes</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
                            
					<article class="col-md-3 portfolio-item pf-media pf-uielements">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_sltg/9sltg.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/9sltg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Sambutan Oleh Direktur Utama Bapak Edwin Susanto</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-uielements">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_sltg/10sltg.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/10sltg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Fokus Calon Konsultan Efes Saat Ada Sambutan Dari Direktur Utama EFES</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-uielements">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_sltg/11sltg.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/11sltg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Penjelasan Marketing Plan Oleh Bapak Nur Cahyo Riswanto</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-uielements">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_sltg/12sltg.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/12sltg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Pemutaran Video untuk Memotivasi Calon Konsultan EFES</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-uielements">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_sltg/13sltg.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/13sltg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Demo Product EFES</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-uielements">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_sltg/14sltg.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/14sltg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Demo Product Efes</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-uielements">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_sltg/15sltg.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/15sltg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Kasir Untuk Calon Konsultan Yang Mau Join</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-uielements">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_sltg/16sltg.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/16sltg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Foto Bersama Team EFES</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
                                        <!--BOP SEMARANG-->
					
                                           <article class="col-md-3 portfolio-item pf-media pf-smrg">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_smrg/6smrg.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/6smrg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Fokus Calon Konsultan Pada Penjelasa Product Knowledge</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
					
					<article class="col-md-3 portfolio-item pf-media pf-smrg">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_smrg/7smrg.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/7smrgg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Ramainya Tamu Undangan Yang Antusias Hadir</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-smrg">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_smrg/8smrg.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/8smrg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Antusias Calon Konsultan Efes</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
                            
					<article class="col-md-3 portfolio-item pf-media pf-smrg">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_smrg/9smrg.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/9smrg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Sambutan Oleh Direktur Utama Bapak Edwin Susanto</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-smrg">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_smrg/10smrg.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/10smrg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Fokus Calon Konsultan Efes Saat Ada Sambutan Dari Direktur Utama EFES</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-smrg">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_smrg/11smrg.jpg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/11smrg.jpg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Penjelasan Marketing Plan Oleh Bapak Nur Cahyo Riswanto</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
                            
                                <!--BOP JAKARTA-->
					<article class="col-md-3 portfolio-item pf-media pf-jkt">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_jkt/1jkt.jpeg" alt="Stockist">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/1jkt.jpeg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Pembukaan Acara BOP oleh Ibu Direktur Rosyani S.H</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>

					<article class="col-md-3 portfolio-item pf-jkt">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_jkt/2jkt.jpeg" alt="Office">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/2jkt.jpeg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Antusias Calon Konsultan Efes</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>

					<article class="col-md-3 portfolio-item pf-graphics pf-jkt">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_jkt/3jkt.jpeg" alt="re">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/3jkt.jpeg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Antusias Calon Konsultan Efes</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-jkt">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_jkt/4jkt.jpeg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
							<!-- data-lightbox="iframe" -->
								<a href="assets/images/gallery/4jkt.jpeg" class="center-icon" data-lightbox="image"><i class="icon-line-play"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Calon Konsultan EFES Fokus Saat Pembukaan Acara BOP</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
                                        <article class="col-md-3 portfolio-item pf-graphics pf-media pf-jkt">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_jkt/5jkt.jpeg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
							<!-- data-lightbox="iframe" -->
								<a href="assets/images/gallery/5jkt.jpeg" class="center-icon" data-lightbox="image"><i class="icon-line-play"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Penjelasan Product Knowledge Oleh dr. Narto</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
                                           <article class="col-md-3 portfolio-item pf-media pf-jkt">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_jkt/6jkt.jpeg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/6jkt.jpeg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Fokus Calon Konsultan Pada Penjelasa Product Knowledge</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
					
					<article class="col-md-3 portfolio-item pf-media pf-jkt">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_jkt/7jkt.jpeg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/7jkt.jpeg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Ramainya Tamu Undangan Yang Antusias Hadir</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-jkt">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_jkt/8jkt.jpeg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/8jkt.jpeg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Antusias Calon Konsultan Efes</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
                            
					<article class="col-md-3 portfolio-item pf-media pf-jkt">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_jkt/9jkt.jpeg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/9jkt.jpeg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Sambutan Oleh Direktur Utama Bapak Edwin Susanto</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-jkt">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_jkt/10jkt.jpeg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/10jkt.jpeg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Fokus Calon Konsultan Efes Saat Ada Sambutan Dari Direktur Utama EFES</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-jkt">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_jkt/11jkt.jpeg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/11jkt.jpeg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Penjelasan Marketing Plan Oleh Bapak Nur Cahyo Riswanto</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
					<article class="col-md-3 portfolio-item pf-media pf-jkt">
						<div class="portfolio-image">
							<a href="#">
								<img src="assets/images/gallery/mini/bop_jkt/12jkt.jpeg" alt="Coming Soon..">
							</a>
							<div class="portfolio-overlay">
								<a href="assets/images/gallery/12jkt.jpeg" class="center-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
								<!--<a href="#" class="right-icon"><i class="icon-line-ellipsis"></i></a>-->
							</div>
						</div>
<!--						<div class="portfolio-desc">
							<h3>Penjelasan Marketing Plan Oleh Bapak Nur Cahyo Riswanto</h3>
							<span>BOP di Hotel Grand Wahid Salatiga</span>
						</div>-->
					</article>
				</div><!-- #portfolio end -->
                        
			</div>

		</section><!-- #content end -->

<?php
include "footer.php";
?>

<!--<script>
var slideIndex = 1;
showDivs(slideIndex);

function plusDivs(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("mySlides");
  if (n > x.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = x.length}
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";  
  }
  x[slideIndex-1].style.display = "block";  
}
</script>-->