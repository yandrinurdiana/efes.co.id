<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="CIC-WebPro" />

	<!-- Stylesheets
	============================================= -->
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="assets/main.css" type="text/css" />
        	<link rel="stylesheet" href="assets/css/subscribe.css" type="text/css" />
	<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
	<!-- <link rel="stylesheet" href="/assets/particles/css/style.css" type="text/css"/> -->
	<link rel="stylesheet" href="assets/css/swiper.css" type="text/css" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->

	<!-- Document Title
	============================================= -->
	<title>EFES NETWORK</title>

	<style type="text/css">
		.button-home {
    
    height: 45px !important;
    
}
	</style>


</head>

<body class="stretched">

<?php
define('TIMEZONE', 'Asia/Jakarta');
?>
		<!-- Header
		============================================= -->
		<header id="header" class="transparent-header">
			<div id="header-wrap">
				<div class="container clearfix">
					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

					<!-- Logo
					============================================= -->
					<div id="logo">
						<a href="/"><img src="assets/images/logo.png" alt="efes Logo"></a>
					</div><!-- #logo end -->

<?php
include "navbar.php";
?>
				</div>
			</div>
		</header><!-- #header end -->